// server.js
// where your node app starts

// we've started you off with Express (https://expressjs.com/)
// but feel free to use whatever libraries or frameworks you'd like through `package.json`.
const express = require('express');
const app = express();

// https://expressjs.com/en/starter/basic-routing.html
app.get('/', (request, response) => {
  response.send('I love CodersX');
});
var todos = [  
             {
               id: 1,
               content: 'Đi chợ'
             },
             {
               id: 2,
               content: 'Nấu cơm'
             }
             ,
             {
               id: 3,
               content: 'Rửa bát'
             }
               ,
             {
               id: 4,
               content: 'Học code tại CodersX'
             }

]
app.get('/todos',(req,res)=> {
  let html = '<ul>';
  for (let item of todos) {
    html += `<li>${item.content}</li>`;
  }
  html+='</ul>';
  res.send(html);
});
// listen for requests :)
app.listen(process.env.PORT, () => {
  console.log("Server listening on port " + process.env.PORT);
});
